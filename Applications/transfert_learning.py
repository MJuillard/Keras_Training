# the code below aims to recover the weight of a pretrained keras model and then to adapt some layers (i.e we apply transfert learning technichs). Here it's the VGG16 model
from keras.applications.vgg16 import VGG16
classifier = VGG16()

# names of the layers
last_layer = str(classifier.layers[-1])

# Building of a new sequential keras model.
model_2 = Sequential()

# Then we browse the different layers of the classifier which will transfer its weights. 
# As soon as the name of a layer is different than the names of the layers we want to retune we add it to the new sequential model. 
for layer in classifier.layers:
    if str(layer)!=last_layer:
        model_2.add(layer)

# After that we indicate to keras that the layer previously add don't have to be trainable :
for layer in model_2.layers:
    layer.trainable = False
    
# To conclude we add some new layers (here at the end). As this layers hasn't the trainable caracteristique to False they will be update
model_2.add(Dense(1,activation='sigmoid'))
