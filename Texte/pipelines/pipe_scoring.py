#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    TITLE
"""

__author__ = "Yolan Honore-Rouge"
__version__ = "0.1"
__email__ = "yolan.honore-rouge@socgen.com"
__status__ = "Development"

import jobs.job_ingest as job_ingest
import jobs.job_preprocess as job_preprocess
import jobs.job_predict_nlp as job_predict_nlp
import jobs.job_predict_ml as job_predict_ml

def scoring(json_data,
            punctuation, accents, stopwords_file, spellchecker_file, postagger_file,
            tokenizer, label_encoder, tokenizer_mode,
            models_store):

    # extract emails
    mails = job_ingest.ingest_data(json_data)

    job_preprocess.preprocess(mails = mails,
                              punctuation = punctuation,
                              accents = accents,
                              stopwords_file = stopwords_file,
                              spellchecker_file = spellchecker_file,
                              postagger_file = postagger_file)

    job_predict_nlp.predict_nlp(mails = mails,
                              tokenizer = tokenizer,
                              label_encoder = label_encoder,
                              tokenizer_mode = tokenizer_mode)

    job_predict_ml.predict_ml(mails, models_store = models_store)

    return mails


