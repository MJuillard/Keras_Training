#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    TITLE
"""

__author__ = "Yolan Honore-Rouge"
__version__ = "0.1"
__email__ = "yolan.honore-rouge@socgen.com"
__status__ = "Development"

import objects.collections as collection
# import jobs.job_extract as job_extract
import jobs.job_preprocess as job_preprocess
import jobs.job_train_nlp as job_train_nlp
import jobs.job_ingest as job_ingest


def training_nlp(json_data,
                 punctuation, accents, stopwords_file, spellchecker_file, postagger_file,
                 num_words, oov_token):

    # extract emails
    mails = job_ingest.ingest_data(json_data)

    job_preprocess.preprocess(mails=mails,
                              punctuation=punctuation,
                              accents=accents,
                              stopwords_file=stopwords_file,
                              spellchecker_file=spellchecker_file,
                              postagger_file=postagger_file)

    tokenizer, label_encoder = job_train_nlp.train_nlp_model(mails=mails,
                                                             num_words=num_words,
                                                             oov_token=oov_token)

    return tokenizer, label_encoder

    # import objects.preprocessing_functions
    # tokenizer, label_encoder = training_nlp(folder_path = r"B:\ASSU-RCF-GSC-SCE\SCE GED\TEST POC\demarrage du POC\1A-Catégorisation- jeu entrainement mail sur 7 Categories\PO SINISTRE",
    #                                                recursive = True,
    #                                                parent_folder_as_label = True,
    #                                                punctuation = objects.preprocessing_functions.PUNCTUATION,
    #                                                accents = objects.preprocessing_functions.ACCENTS,
    #                                                stopwords_file = None,
    #                                                spellchecker_file = None,
    #                                                postagger_file = None,
    #                                                num_words = 2000,
    #                                                oov_token="unknown")
    #
