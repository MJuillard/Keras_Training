#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    TITLE
"""

__author__ = "Yolan Honore-Rouge"
__version__ = "0.1"
__email__ = "yolan.honore-rouge@socgen.com"
__status__ = "Development"

import objects.collections as collection
import jobs.job_preprocess as job_preprocess
import jobs.job_predict_nlp as job_predict_nlp
import jobs.job_train_ml as job_train_ml
import jobs.job_ingest as job_ingest
import logging

logger = logging.getLogger('catmail')


def training_ml(json_data, punctuation, accents, stopwords_file, spellchecker_file, postagger_file,
                tokenizer, label_encoder, tokenizer_mode, target_classes, parameters_space):

    # extract emails
    mails = job_ingest.ingest_data(json_data)

    job_preprocess.preprocess(mails=mails,
                              punctuation=punctuation,
                              accents=accents,
                              stopwords_file=stopwords_file,
                              spellchecker_file=spellchecker_file,
                              postagger_file=postagger_file)

    job_predict_nlp.predict_nlp(mails=mails,
                                tokenizer=tokenizer,
                                label_encoder=label_encoder,
                                tokenizer_mode=tokenizer_mode)
    if not target_classes:
        target_classes = label_encoder.classes_

    # The parameters of the model should probably depends on the distribution of the data
    # It is very likely that we should infer it from job_train_nlp
    model_kwargs = dict(
        nb_classes=len(label_encoder.classes_),
        embedding_dim=150,
        vocabulary_size=tokenizer.num_words,
        max_sentence_length=150, # infer it from the shape of the sentence list
        gru_units=,
        filters=,
        kernel_size=,
        out_activation
    )

    models_store = {}
    for target_label in target_classes:
        logger.info("TRAINING : {}".format(target_label))
        models_store[target_label] = job_train_ml.train_model(mails=mails,
                                                              target_label=target_label,
                                                              parameters_space=parameters_space)

    return models_store
