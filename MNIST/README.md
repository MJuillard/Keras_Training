# MNIST

## Packages nécessaires
- keras : permettra de charger la base MNIST,
    - keras.sequential de construire le model de deep learning.
    - keras.Dense et keras.Dropout permettra de tuner le RN
        
## Pretraitement
### base train
On charge le base MNIST train et test : 60 000 et 10 0000 image de 28 sur 28 pixels. La matrice 28x28 est ensuite transformée en vecteur de taille 764 (28x28).
### base test
La variable Y est prétraitée par onehot encoding : *utils.to_categorical(y_train, n_classes)*. Au lieu de disposer de nombre, on dispose d'un vesteur de taille 10 (pour chaque observation), avec une coordonnée à 1 pour détecter le bon nombre.

## Construction du modèle
La fonction Sequential() de keras permet de construire un RN vide. On paramètre ce dernier par la suite.

    model = Sequential()
    # the first layer has to specify the dimensions of the input vector
    model.add(Dense(units=128, activation='sigmoid', input_shape=(n_inputs,)))
    # add dropout layer for preventing overfitting
    model.add(Dropout(0.1))
    model.add(Dense(units=128, activation='sigmoid'))
    model.add(Dropout(0.1))
    # output layer can only have the neurons equal to the number of outputs
    model.add(Dense(units=n_classes, activation='softmax')) 

- model.add() permet de venir ajouter des couches
    
    - Dense() permet d'ajouter une couche de neurone *fully connected*.
        
        - activation : permet d'indiquer la fonction d'activation souhaitée : sigmoide, tanh, linear, softmax, softplus, softsign, relu, hard_sigmoid,elu, selu. 
        - input_shape(,) ?
        - units : nombre de neurone.
        
    - Dropout() renseigne le taux de neurone qui seront désactivé à chaque entrainement.
    
model.summary permet d'obtenir un résumé du RN ainsi que le nombre de paramètre à ajuster :
    Layer (type)                 Output Shape              Param #   
    =================================================================
    dense_2 (Dense)              (None, 128)               100480    
    _________________________________________________________________
    dropout_3 (Dropout)          (None, 128)               0         
    _________________________________________________________________
    dense_3 (Dense)              (None, 128)               16512     
    _________________________________________________________________
    dropout_4 (Dropout)          (None, 128)               0         
    _________________________________________________________________
    dense_4 (Dense)              (None, 10)                1290      
    =================================================================
    
## Compilation du modèle
Le modèle défini précédemment est compilé au travers des lignes de codes suivantes :

    -model.compile(loss='categorical_crossentropy',
              optimizer=SGD(),
              metrics=['accuracy'])      
        
   - loss : renseigne la fonction de perte à optimiser : mean_square_error, mean_absolute_error, mean_absolute_pecentage_error,mean_squared_logarithmic_error, square_hinge, categorical_hinge,sparse_categorical_crossentropy, binary_crossentropy, poisson, cosine_proximity, binary_accuracy, roc_auc_score, categorical_crossentropy, binary_crossentropy, softymax_categorical_crossentropy, weighted_crossentropy, hinge_loss, weak_cross_entropy_2d.
   - optimiser : fonction utiliser pour mettre à jour les paramètres. Keras intègre les 7 optimiser suivants : SGD, RMSprop, Adagrad, Adadelta, Adam, Adamax, Nadam
   - metric : accuracy,
## Entrainement du modèle
        model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=n_epochs) 
   - batch_size = nombre de ligne de l'échantillon pour chauqe paquet d'entrainement (si 100 et echantillon de taille 1000 alors 10 paquets de 100) 
   - epoch = nombre de passage de l'échantillon pour l'ajustement du modèle.       
