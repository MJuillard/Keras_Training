# Skipgram pour creer les exemples alimentant l'embedding
skipgrams_generator <- function(text, tokenizer, window_size, negative_samples) {
  gen <- texts_to_sequences_generator(tokenizer, sample(text))
  function() {
    skip <- generator_next(gen) %>%
      skipgrams(
        vocabulary_size = tokenizer$num_words, 
        window_size = window_size, 
        negative_samples = 1
      )
    x <- transpose(skip$couples) %>% map(. %>% unlist %>% as.matrix(ncol = 1))
    y <- skip$labels %>% as.matrix(ncol = 1)
    list(x, y)
  }
}

# Tokenizer (dictionnaire de mots)
tokenizer = function(data,num_words) {
  temp <- text_tokenizer(num_words)
  temp %>% fit_text_tokenizer(data)
  return(temp)
}
