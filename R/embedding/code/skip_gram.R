# download.file("https://snap.stanford.edu/data/finefoods.txt.gz", "finefoods.txt.gz")
# Chargement des données ----
library(readr)
library(stringr)
reviews <- read_lines("finefoods.txt.gz") 
reviews <- reviews[str_sub(reviews, 1, 12) == "review/text:"]
reviews <- str_sub(reviews, start = 14)
reviews <- iconv(reviews, to = "UTF-8")
  
# Tokenisation ----
library(keras)
tokenizer <- text_tokenizer(num_words = 20000)
tokenizer %>% fit_text_tokenizer(reviews)

# Construction du générateur skipgram ----
library(reticulate)
library(purrr)
skipgrams_generator <- function(text, tokenizer, window_size, negative_samples) {
  gen <- keras::texts_to_sequences_generator(tokenizer, sample(text)) 
  # Transforme le text en séquence d'entier en se basant sur le tokenizer précédemment construit.
  # Seuls les mots renseignés dans le Tokeniser seront pris en compte.
  # https://www.rdocumentation.org/packages/keras/versions/2.2.0/topics/texts_to_sequences_generator
  function() {
    skip <- generator_next(gen) %>%
      keras::skipgrams(
        vocabulary_size = tokenizer$num_words, 
        window_size = window_size, 
        negative_samples = 1 # valeur par défaut, construit une base équilibré pour le train
      )
      # le pip envoi la séquence de nombres crées précédemment dans la fonction skipgram (remplit la variable sequence)
      # la fonction skipgram va créer les échantillons d'entrainement : 
        # mot cible, mot de context et une variable (0,1) indiquant si le package mot/contexte est vrai ou si c'est un faux exemple
    x <- transpose(skip$couples) %>% map(. %>% unlist %>% as.matrix(ncol = 1))
    y <- skip$labels %>% as.matrix(ncol = 1)
    list(x, y)
  }
}

embedding_size <- 128  # Dimension of the embedding vector.
skip_window <- 5       # How many words to consider left and right.
num_sampled <- 1       # Number of negative examples to sample for each word.

# 1) Définition de la couche d'entrée ----
input_target <- layer_input(shape = 1)
input_context <- layer_input(shape = 1)
# Layer input : couche utilisée comme input à un graph 
# Shape indique la dimension de l'entrée. Ici nous somme en 1 dimension (un indexe de mot)

# 2) Couche d'embedding ----
embedding <- layer_embedding(
  input_dim = tokenizer$num_words + 1, 
  output_dim = embedding_size, 
  input_length = 1, 
  name = "embedding"
)
# layer_embedding : couche dans lequel se trouve les poids de l'embeddging
# input dim : nombre de lignes = taille du vocabulaire +1
# output dim : nombre de colonnes = taille de l'embedding
# input length : dimention des inputs => shape
# name : nom de la couche d'embedding

target_vector <- input_target %>% 
  embedding() %>% 
  layer_flatten()

context_vector <- input_context %>%
  embedding() %>%
  layer_flatten()
# Le mot en input passe dans la couche d'embedding puis dans la couche layer
# Même traitement l'input contexte

dot_product <- layer_dot(list(target_vector, context_vector), axes = 1)
# Produit scalaire

output <- layer_dense(dot_product, units = 1, activation = "sigmoid")
# Couche de sortie avec activation sigmoid. 
# C'entrée est la sortie du produit scalaire.
# Sortie en dimension 1
# output contient donc le produit scalaire, qui contient le vector target et context.

model <- keras_model(list(input_target, input_context), output)
# On rensigne le modèle. output contient tout le code. List() renseigne les deux entrées.

model %>% compile(loss = "binary_crossentropy", optimizer = "adam")
summary(model)


model %>%
  fit_generator(
    skipgrams_generator(reviews, tokenizer, skip_window, negative_samples), 
    steps_per_epoch = 100000, epochs = 5
  )


# Utilisation du word embedding

library(dplyr)
# Extraction des poids
embedding_matrix <- get_weights(model)[[1]] 

# AJout des noms au poids
words <- data_frame(
  word = names(tokenizer$word_index), 
  id = as.integer(unlist(tokenizer$word_index))
)

words <- words %>%
  filter(id <= tokenizer$num_words) %>%
  arrange(id)

row.names(embedding_matrix) <- c("UNK", words$word)


library(text2vec)

find_similar_words <- function(word, embedding_matrix, n = 5) {
  similarities <- embedding_matrix[word, , drop = FALSE] %>%
    sim2(embedding_matrix, y = ., method = "cosine")
  
  similarities[,1] %>% sort(decreasing = TRUE) %>% head(n)
}

library(Rtsne)
library(ggplot2)
library(plotly)

tsne <- Rtsne(embedding_matrix[2:500,], perplexity = 50, pca = FALSE)

tsne_plot <- tsne$Y %>%
  as.data.frame() %>%
  mutate(word = row.names(embedding_matrix)[2:500]) %>%
  ggplot(aes(x = V1, y = V2, label = word)) + 
  geom_text(size = 3)
tsne_plot