# Keras_Training

## Objet
Ce Git a pour objectif de présenter les différents codes construit pour se former au **deep learning** sous Keras. Il s'appuit notamment sur le livre Mastering TensorFlow 1x.

## Plan 

<h1>Table of Contents<span class="tocSkip"></span></h1>
<div class="toc" style="margin-top: 1em;"><ul class="toc-item"><li><span><a href="#Keras-MNIST-Example" data-toc-modified-id="Keras-MNIST-Example-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Keras MNIST Example</a></span></li></ul></div>
