def get_metrics(y_pred, y_test, decimals=3):
    """
    Computes metrics
    :param y_pred: Predicted labels.
    :param y_test: Ground truth labels.
    :param decimals: Decimal precision of metric.
    :return: Dctionary of statistics and computations.
    """
    f1_metric = sklearn.metrics.f1_score(y_test, y_pred)
    confusion = confusion_matrix(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    print("----------------------")
    print(f1_metric)
    statistics = {
        'f1_score': round(f1_metric, decimals),
        'precision': round(precision, decimals),
        'recall': round(recall, decimals),
        'confusion_matrix': confusion
    }
    return statistics