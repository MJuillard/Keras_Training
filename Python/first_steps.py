# library os

# Change le répertoire de travail actuel par path.
    os.chdir(path)

#Renvoie une chaîne de caractères représentant le répertoire de travail actuel.
    os.getcwd()

# Concaténer des chemins
    os.path.join(path,"Test_programme\\job_predict_nlp")

# Pour charger un programme qui n'est pas dans le dossier mère, il faut ajouter un fichier __init__.py
    from __future__ import absolute_import
    from . import *

# Library d'aide
    import logging
    # Exemple
    logging.basicConfig(filename='example.log',level=logging.DEBUG)
    logging.debug('This message should go to the log file')
    logging.info('So should this')
    logging.warning('And this, too')

# json
    import json