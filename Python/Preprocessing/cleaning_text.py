#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    This file contains all preprocessing functions needed for cleaning
"""

import re
import unidecode
import pandas as pd
import bs4  # BeautifulSoup for HTML parsing
import numpy as np
import spacy
from french_lefff_lemmatizer.french_lefff_lemmatizer import FrenchLefffLemmatizer

__author__ = "Yolan Honore-Rouge"
__version__ = "0.1"
__email__ = "yolan.honore-rouge@socgen.com"
__status__ = "Development"

ACCENTS = {'a': "àáâãäå",
           'e': "èéêë",
           'i': "ìíîï",
           'o': "òóôõö",
           'u': "ùúûü",
           'y': "ýÿ"}

PUNCTUATION = {
    r"\!": "",
    r"\?": "",
    r"\"": " ",  # space
    r"\'": " ",  # space
    r"\’": " ",  # space
    r"#": " ",  # space
    r"\$": " ",  # space
    r"\%": " ",  # space
    r"\&": " ",  # space
    r"\(": " ",  # space
    r"\)": " ",  # space
    r"\*": " ",  # space
    r"\+": " ",  # space
    r"\,": " ",  # space
    r"\-": "",
    r"\.": " ",  # space
    r"/": "",
    r"\\": "",
    r":": " ",  # space
    r";": " ",  # space
    r"\<": "",
    r"\=": " ",  # space
    r"\>": "",
    r"\@": "",
    r"\[": " ",  # space
    r"\]": " ",  # space
    r"\^": "",
    r"_": "",
    r"`": " ",  # space
    r"\{": "",
    r"\|": "",
    r"\}": "",
    r"~": "",
    r"\t": " ",  # space
    r"\n": " ",  # space
    r"\°": " "
}

FRENCH_DICTIONARY_PATH = "2_DONNEES/2_PREPROCESSING_PARAMETERS/freq_fr.txt"

# SPACY_TO_LEFFF ={'ADJ': 'adj',
# 					'ADP': 'det',
# 					'ADV': 'adv',
# 					'AUX': 'v',
# 					'DET': 'det',
# 					'PRON': 'pro',
# 					'PROPN': 'np',
# 					'NOUN': 'nc',
# 					'VERB': 'v',
# 					'PUNCT': 'poncts'
# 					}
SPACY_LEFFF_TABLE = np.asarray([
    ['adj', 'ADJ'],
    ['adv', 'ADV'],
    ['prep', 'ADP'],
    ['nc', 'NOUN'],
    ['np', 'PROPN'],
    ['ver', 'VERB'],
    ['v', 'VERB'],
    ['auxAvoir', 'AUX'],
    ['auxEtre', 'AUX'],
    ['poncts', 'PUNCT'],
    ['cln', 'PRON'],
    ['cld', 'PRON'],
    ['clar', 'PRON'],
    ['cla', 'PRON'],
    ['cll', 'PRON'],
    ['clr', 'PRON'],
    ['pro', 'PRON'],
    ['det', 'DET']
])

""" FUNCTIONS """


# removing punctuation in a given list from a string
def remove_punctuation(string, punctuation):
    # first create a dict with all replacement as keys, and values with this replacement as values
    reverse_puncts = {}
    for key, value in sorted(punctuation.items()):
        reverse_puncts.setdefault(value, []).append(key)
    # apply all replacements
    for k in reverse_puncts.keys():
        current_pattern = r"(?:" + "|".join(reverse_puncts[k]) + ")"
        string = re.sub(current_pattern, k, string)

    return string


# removing accents from a string
def remove_accents(string, accents = ACCENTS):
    # have to specify key and value must be interpreted as regex
    for k, v in accents.items():
        v_re = r"[" + v + "]"
        string = re.sub(v_re, k, string)
    return string


# removing stopwords
def remove_stopwords(string, stopwords):
    # have to specify key and value must be interpreted as regex
    string = re.sub(r'\b[ ]*(' + '|'.join(stopwords) + r')[ ]*\b', ' ', string)
    return string


def remove_blanks(string):
    string = " ".join(string.split())
    return (string)


def check_spelling(string, spellchecker, max_edit_distance = 2):
    # much faster if it deals with short string (words) rather than long string (sentence
    # corrected_sentence = spellchecker.lookup_compound(string, max_edit_distance=max_edit_distance)
    # corrected_sentence = [word.term for word in corrected_sentence][0]
    corrected_sentence = " ".join([correct.term for word in string.split() for correct in
                                   spellchecker.lookup(word, max_edit_distance = max_edit_distance, verbosity = 0)])
    return corrected_sentence


def spacy_to_lefff(spacy_tag):
    """
    Permet de convertir un tag Spacy en tag utilisé par LEFFF.
    :param spacy_tag: Le tag Spacy.
    :return: Tag au format de LEFFF.
    """
    for row in SPACY_LEFFF_TABLE:
        if row[1] == spacy_tag:
            return row[0]
    return spacy_tag


def select_best_lemma(lemmas):
    # lemmas_dict = {}
    # for k, v in dict(lemmas).items():
    # 	lemmas_dict.setdefault(v, k)
    # # a vue de nez, on classe par ordre de probabilite (les tests empiriques montrent que cest mieux que prendre la premiere valeur au hasard)
    #
    # for k in ["auxEtre", "auxAvoir", "v", "ver", "nc", "adj"]:
    # 	if k in lemmas_dict.keys():
    # 		return lemmas_dict[k]
    # return lemmas[0][0]
    lemmas_dict = {t[1]: t[0] for t in lemmas}
    best_lemma = None
    for postag in ["auxEtre", "auxAvoir", "v", "ver", "nc", "adj", "prel"]:
        best_lemma = lemmas_dict.get(postag)
        if best_lemma is not None:
            break
    # if no best lemma is found, default to first value
    if best_lemma is None:
        best_lemma = lemmas[0][0]
    return best_lemma


def lemmatize(string, postagger, lemmatizer):
    """
    Racinisation des sujets des mails.
    """
    # try:
    if len(string) <= 2:  # On ne considère pas de racinisation sur les phrases de taille inferieures à 3
        return string
    doc = postagger(string)
    lemmas = []

    for token in doc:  # Pour chaque token pos-tagged
        word_lemmas = lemmatizer.lemmatize(token.text,
                                           spacy_to_lefff(token.pos_))  # look for the lemma associated to the right tag
        if not isinstance(word_lemmas, list):
            word_lemmas = [word_lemmas]  # convert to list, because "noms propres" are dealt differently with spacy

        lemma = token.text
        if len(word_lemmas) > 0:
            try:  # some errors can occur, did not undestand why. Likely too weirds string which cannot be lemmatized
                lemma = select_best_lemma(
                    word_lemmas)  # Si on a trouve au moins un tag (il peut y en avoir plusieurs), on selectionne le meilleur
            except:
                pass  # Par défaut, le lemme est le texte
        lemmas.append(lemma)
    lemmatized_sentence = " ".join(lemmas)
    # except:
    # 	raise Warning("Impossible de raciniser le sujet du mail d'ID")
    return lemmatized_sentence


def parse_html_body(string):
    soup = bs4.BeautifulSoup(string,
                             features = "html.parser")  # html5lib: known bug https://stackoverflow.com/questions/23113803/python-beautiful-soup-nonetype-object-error
    html_text = [tag.text for tag in soup.find_all(name = ['span', 'p'], recursive = True, text = False)]
    html_text = " ".join(html_text)
    html_text = html_text.replace(u'\xa0', u' ')  # breakline in latin1 encoding
    return (html_text)


def clean_string(max_edit_distance, string, punctuation, accents, stopwords, spellchecker, postagger, lemmatizer,
                 **kwargs):
    if "max_edit_distance" not in kwargs.keys():
        max_edit_distance = 2
    mail_regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    website_regex = r"\b(?:http[s]?://(?:www\.)?|www\.).*\.(?:fr|org|ai|net|io|com)"
    phone_regex = r"(?:\+33[ ]?(?:\(0\)|0)?|0)[ ]?[1-9](?:[-. ]?(?:\d{2}[-. ]?)){4}"

    string = string.lower()  # transform to lower values
    string = re.sub(mail_regex, "ADRESSE_MAIL", string)
    string = re.sub(website_regex, "SITEINTERNET", string)
    string = re.sub(phone_regex, "NUMEROTELEPHONE", string)
    string = re.sub(r"\[::(?:break|line)::\]", " ",
                    string)  # cannot be dealt simultaneously with punctuation : conflicts with [:: removed first
    string = remove_punctuation(string = string, punctuation = punctuation)  # include special values when parsed in C#
    # must deal with numbers before spell checking
    if spellchecker:
        string = check_spelling(string = string, spellchecker = spellchecker,
                                max_edit_distance = max_edit_distance)  # include special values when parsed in C#

    # concerts string to unicode # do weirds things with special character

    if postagger and lemmatizer:
        string = lemmatize(string, postagger, lemmatizer)  # include special values when parsed in C#
    string = remove_accents(string = string, accents = accents)
    if stopwords:
        string = remove_stopwords(string = string, stopwords = stopwords)
    string = remove_blanks(string)
    string = unidecode.unidecode(string)
    return string
