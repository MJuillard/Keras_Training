def clean_data(dataset_df, column_to_clean, stopwords_lst, stemmer = None):
    """
    This function cleans the column `column_to_clean` from the dataset `dataset_df`.

    --------------------------------------------------------------------------------
    Arguments:
    --------------------------------------------------------------------------------
    dataset_df: Pandas DataFrame
            DataFrame were the data is stored.
    
    column_to_clean: string
            Name of the column we want to clean.
            
    stopwords_lst: list
            List of stopwords we want to remove.
            
    stemmer: NLTK stemmer (default = None)
            Stemmer to be used to stem words. If None, no stemming is performed.

    --------------------------------------------------------------------------------
    Returns:
    --------------------------------------------------------------------------------
    dataset_df: Pandas DataFrame
            DataFrame were the cleaned data is stored.
    """
    
    # Start a stopwatch to measure the time taken by the function to execute itself.
    start_time = time.time()
    
    # Remove remaining HTML tags
    dataset_df["review"] = dataset_df["review"].str.replace("<.*?>", " ")
    
    # Remove all numbers
    dataset_df["review"] = dataset_df["review"].str.replace("(?:[+-]|\()?\$?\d+(?:,\d+)*(?:\.\d+)?\)?", " ")
    
    # Remove punctuation
    dataset_df["review"] = dataset_df["review"].str.replace("[" + string.punctuation + "]", " ")
    
    # Remove all characters that are not letters
    dataset_df["review"] = dataset_df["review"].str.replace("[^a-zA-Z\ ]", " ")
    
    # Convert the text to lower case
    dataset_df["review"] = dataset_df["review"].str.lower()
    
    # Remove stopwords
    dataset_df["review"] = dataset_df["review"].str.replace("(\s+|^)(" + "|".join(stopwords_lst) + ")(\s+|$)", " ")
    
    # Remove large spaces
    dataset_df["review"] = dataset_df["review"].str.replace("\s+", " ")
    
    # Remove leading and trailing spaces
    dataset_df["review"] = dataset_df["review"].str.replace("\s+$|^\s+", "")
    
    # Remove word only made of one letter
    dataset_df["review"] = dataset_df["review"].str.replace("(\s+|^)([a-z]{1})(\s+|$)", " ")
    
    # Stem all words
    if stemmer is not None:
        dataset_df["review"] = dataset_df["review"].apply(lambda x: " ".join([stemmer.stem(w) for w in x.split(" ")]))
    
    print(dataset_df.shape[0], "samples cleaned in", round(time.time() - start_time, 3), "secs")
    
    return dataset_df
