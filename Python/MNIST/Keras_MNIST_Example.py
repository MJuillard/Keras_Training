import sys
#!{sys.executable} -m pip install --upgrade keras
!{sys.executable} -m pip install -- quiver_engine


import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import SGD
from keras import utils
import numpy as np
import theano


# define some hyper parameters
Batch_size = 100
n_inputs = 784
n_classes = 10
N_epochs = 10
Validation_split=0.3

# get the data

(x_train, y_train), (x_test, y_test) = mnist.load_data()

# reshape the two dimensional 28 x 28 pixels
#   sized images into a single vector of 784 pixels
x_train = x_train.reshape(60000, n_inputs)
x_test = x_test.reshape(10000, n_inputs)

# convert the input values to float32
x_train = x_train.astype(np.float32)
x_test = x_test.astype(np.float32)

# normalize the values of image vectors to fit under 1
 x_train = x_train/ 255
 x_test  =x_test/ 255

# convert output data into one hot encoded format
y_train = utils.to_categorical(y_train, n_classes)
y_test = utils.to_categorical(y_test, n_classes)

# build a sequential model
model = Sequential()
# the first layer has to specify the dimensions of the input vector
model.add(Dense(units=128, activation='relu', input_shape=(n_inputs,)))
# add dropout layer for preventing overfitting
model.add(Dropout(0.1))
model.add(Dense(units=128, activation='sigmoid'))
model.add(Dropout(0.1))
# output layer can only have the neurons equal to the number of outputs
model.add(Dense(units=n_classes, activation='softmax'))

# print the summary of our model
model.summary()

## Plot the model
from keras.callbacks import TensorBoard
tbCallBack = keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=True, write_images=True)

# compile the model
model.compile(loss='categorical_crossentropy',
              optimizer=SGD(),
              metrics=['accuracy'])

# train the model
model.fit(x_train, y_train,
          batch_size=Batch_size,
          epochs=N_epochs,
          validation_split=Validation_split,
          callbacks=[tbCallBack]
          )

# CMD : tensorboard --logdir C:/Users/Utilisateur/Travail/Keras_Training/Graph

# evaluate the model and print the accuracy score
#################################################

model_accuracy_train = np.eye(10,2)
model_accuracy_test = np.eye(10,2)
for epoch in range(1,11):
    model.fit(x_train, y_train,
              batch_size=batch_size,
              epochs=epoch)
    print(epoch)
    model_accuracy_train[epoch-1,]=[model.evaluate(x_train,y_train)[0],model.evaluate(x_train,y_train)[1]]
    model_accuracy_test[epoch-1,]=[model.evaluate(x_test,y_test)[0],model.evaluate(x_test,y_test)[1]]

import matplotlib.pyplot as plt
p1=plt.plot(range(1,11),model_accuracy_test[:,1])
p2=plt.plot(range(1,11),model_accuracy_train[:,1])
plt.title("Evolution accuracy en fonction de l'epoch")
plt.legend([p1, p2], ["test", "train"])
plt.show()

scores = model.evaluate(x_test, y_test)

print('\n loss:', scores[0])
print('\n accuracy:', scores[1])
# Save the model
#################

from keras.models import load_model
model.save('.\MNIST\my_First_MNIST_model.h5')
model2 = load_model('.\MNIST\my_First_MNIST_model.h5')


# Early stopping
##################

# build a sequential model
tbCallBack = keras.callbacks.TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=True, write_images=True)
Early_Stopping = keras.callbacks.EarlyStopping(monitor='val_loss',mode='auto',min_delta=0.002)
# train the model
model.fit(x_train, y_train,
          batch_size=Batch_size,
          epochs=N_epochs,
          validation_split=Validation_split,
          callbacks=[tbCallBack,Early_Stopping]
          )
# CMD : tensorboard --logdir C:/Users/Utilisateur/Travail/Keras_Training/Graph
model.evaluate(x_test, y_test)[0], model.evaluate(x_test, y_test)[1]
model2.evaluate(x_test, y_test)[0], model2.evaluate(x_test, y_test)[1]

# Explore the model

Config = model.get_config()

W_0=model.layers[0].get_weights()
len(W_0)
len(W_0[0])
len(W_0[1])