def creating_training_set(path,Rescale, Shear_range,Zoom_range,Horizontal_flip):
    """
    based on a folder witch contains picture, this function apply some modification and then create the input for a CNN model
    """

    train_datagen = ImageDataGenerator(rescale = Rescale,
                                       shear_range = Shear_range,
                                       zoom_range = Zoom_range,
                                       horizontal_flip = Horizontal_flip)
                                       
    training_set = train_datagen.flow_from_directory(path+'train',
                                                     target_size = (64, 64),
                                                     batch_size = 32,
                                                     class_mode = 'binary')
    return(training_set)
    
# rescale = 1./255 ;  shear_range = 0.2 ; zoom_range = 0.2 ; horizontal_flip = True)
# path = 'C:/Users/F400244/PycharmProjects/CIFAR10/data/Cifar_small/'

