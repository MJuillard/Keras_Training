def load_image_array(path_picture,size_picture):
    '''
    This function take a picture (jpg for exemple) and convert it into array
    '''
    from keras.preprocessing.image import load_img
    from keras.preprocessing.image import img_to_array
    # Load the picture
    new_image = load_img(path_picture,
                         target_size = (size_picture[0],size_picture[1]))
    
    # convert the picture to array format
    new_image_array = img_to_array(new_image)
    
    return(new_image_array)