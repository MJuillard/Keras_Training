def pictures_folder(path,size_picture):
    '''
    load all the pictures of a folder and the category of each picture
    the pictures are then transformed into array (to be past after into the RNN)
    '''
    import os
    dirs = os.listdir(path)
    for folders in dirs:
        # path of the folder
        path_folder = path+'/'+folders
        size_folder = len(os.listdir(path_folder))
        
        # load the first picture of the folder (as an array)
        image_array = [load_image_array(path_picture = path_folder + '/'+ os.listdir(path_folder)[0],
                                        size_picture = size_picture)]
        image_type = [folders]
        
        # load the other pictures of the folder (as an array)
        for i in range(1,size_folder):
            image_array.append(load_image_array(path_picture = path_folder + '/' + os.listdir(path_folder)[i],
                                                size_picture = size_picture))
            image_type.append(folders)
            
    return([image_array,image_type])